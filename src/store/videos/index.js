import axios from 'axios'


export default {

  state: {
    videos: [],
    videoToUpdate: null
  },

  getters: {

    getVideos(state, getters){
      return state.videos
    },

    getVideoToUpdate(state, getters){
      return state.videoToUpdate
    }

  },

  mutations: {

    setVideos(state, payload){
      state.videos = payload
    },

    setVideoToUpdate(state, payload){
      state.videoToUpdate = payload
    },

    addVideo(state, payload){
      state.videos = [...state.videos, payload]
    },

    updateVideo(state, payload){
      state.videos = state.videos.filter(video => video.id != payload.id)
      state.videos = [...state.videos, payload]
    },

    deleteVideo(state, payload){
      state.videos = state.videos.filter(video => video.id != payload.id)
    }

  },

  actions: {

    async fetchVideos(context, authToken){

      axios.defaults.headers.common['Authorization'] = `Bearer ${authToken}`
      axios.get('http://hockeysite.romainbastien.com/admin/videos')
        .then((response) => {
          context.commit('setVideos', response.data)
        })
    },

    videoToUpdate(context, payload){
      context.commit('setVideoToUpdate', payload)
    },

    addVideo(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-video', {
        title: payload.video.title,
        yt_id: payload.video.yt_id
      })
        .then(() => {
          context.commit('addVideo', payload.video)
        })
    },

    updateVideo(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/update-video', {
        id: payload.video.id,
        title: payload.video.title,
        yt_id: payload.video.yt_id
      })
        .then(() => {
          context.commit('updateVideo', payload.video)
        })
    },

    deleteVideo(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/delete-video', {
        id: payload.video.id
      })
        .then(() => {
          context.commit('deleteVideo', payload.video)
        })
    }

  }

}
