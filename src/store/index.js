import Vue from 'vue'
import Vuex from 'vuex'
import news from './news'
import galleries from './galleries'
import videos from './videos'
import standings from './standings'
import players from './players'
import games from './games'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    news: news,
    galleries: galleries,
    videos: videos,
    players: players,
    standings: standings,
    games: games
  }
})
