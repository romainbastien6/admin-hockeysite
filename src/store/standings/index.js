import axios from 'axios'

export default {
  state: {
    teams: [],
    currentTeam: null,
    standingToUpdate: null
  },

  getters: {
    getTeams(state, getters){
      return state.teams
    },

    getStandingToUpdate(state, getters){
      return state.standingToUpdate
    },

    getCurrentTeam(state, getters){
      return state.currentTeam
    }
  },

  mutations: {
    setTeams(state, payload){
      state.teams = payload
    },

    setStandingToUpdate(state, payload){
      state.standingToUpdate = payload
    },

    setCurrentTeam(state, payload){
      state.currentTeam = payload
    },

    addRow(state, payload){
      state.standingToUpdate = [...state.standingToUpdate, payload]
    },

    deleteRow(state, payload){
      state.standingToUpdate = state.standingToUpdate.filter(row => row.id != payload.id)
    }
  },

  actions: {
    fetchTeams(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
      axios.get('http://hockeysite.romainbastien.com/admin/standings')
        .then((response) => {
          context.commit('setTeams', response.data)
        })
        .catch(e => console.log(e))
    },

    fetchStanding(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.get('hhttp://hockeysite.romainbastien.com/admin/get-team-standing/' + payload.team.id)
        .then((response) => {
          context.commit('setStandingToUpdate', response.data)
          context.commit('setCurrentTeam', payload.team)
        })
        .catch(e => console.log(e))
    },

    addRow(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-team-standings', {
        category: context.state.currentTeam.id,
        teamName: payload.row.teamName,
        playedGames: payload.row.playedGames,
        points: payload.row.points,
        goalFor: payload.row.goalFor,
        goalAgainst: payload.row.goalAgainst
      })
        .then((response) => {
          console.log(response.data)
          payload.row.id = response.data
          context.commit('addRow', payload.row)
        })
        .catch(e => console.log(e))
    },

    updateRow(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/update-team-standings', {
        id: payload.row.id,
        teamName: payload.row.teamName,
        playedGames: payload.row.playedGames,
        points: payload.row.points,
        goalFor: payload.row.goalFor,
        goalAgainst: payload.row.goalAgainst
      })
    },

    deleteRow(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/delete-team-standings', {
        id: payload.row.id
      })
        .then((r) => {
          context.commit('deleteRow', payload.row)
        })
        .catch(e => console.log(e))
    }
  }
}
