import axios from 'axios'

export default {
  state: {
    games: [],
    gameToUpdate: null
  },

  getters: {
    getGames(state, getters){
      return state.games
    },

    getGameToUpdate(state, getters){
      return state.gameToUpdate
    }

  },

  mutations: {
    setGames(state, payload){
      state.games = payload
    },

    setGameToUpdate(state, payload) {
      state.gameToUpdate = payload
    },

    setUpdatedGame(state, payload) {
      state.games = state.games.filter(game => game.id != payload.id)
      state.games = [...state.games, payload]
    },

    deleteGame(state, payload) {
      state.games = state.games.filter(game => game.id != payload.id)
    }

  },

  actions: {
    fetchGames(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
      axios.get('https://www.hockeysite.test/admin/games')
        .then((response) => {
          context.commit('setGames', response.data)
        })
    },

    createGame(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-game', {
        date: payload.game.date,
        gameType: payload.game.gameType,
        team_id: payload.game.team_id,
        team1: payload.game.team1,
        team2: payload.game.team2,
        completed: payload.game.completed,
        score1: payload.game.score1,
        score2: payload.game.score2
      })
        .then(() => {
          console.log('upload done')
        })
        .catch((e) => {
          console.log(e)
        })
    },

    updateGame(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/update-game', {
        id: payload.game.id,
        date: payload.game.date,
        gameType: payload.game.gameType,
        team_id: payload.game.team_id,
        team1: payload.game.team1,
        team2: payload.game.team2,
        completed: payload.game.completed,
        score1: payload.game.score1,
        score2: payload.game.score2
      })
        .then(() => {
          context.commit("setUpdatedGame", payload.game)
        })
    },

    deleteGame(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/delete-game', {
        id: payload.game.id,
      })
        .then(() => {
          context.commit("deleteGame", payload.game)
        })
    },

    gameToUpdate(context, payload){
      context.commit('setGameToUpdate', payload)
    }
  }
}
