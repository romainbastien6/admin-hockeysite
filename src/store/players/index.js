import axios from 'axios'

export default {
  state: {
    players: [],
    playerToUpdate: null
  },

  getters: {
    getPlayers(state, getters){
      return state.players
    },

    getPlayerToUpdate(state, getters){
      return state.playerToUpdate
    }
  },

  mutations: {
    setPlayers(state, payload){
      state.players = payload
    },

    setPlayerToUpdate(state, payload){
      state.playerToUpdate = payload
    },

    setUpdatedPlayer(state, payload) {
      state.players = state.players.filter(player => player.id != payload.id)
      state.players = [...state.players, payload]
    },

    deletePlayer(state, payload) {
      state.players = state.players.filter(player => player.id != payload)
    }
  },

  actions: {
    fetchPlayers (context, payload) {
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
      axios.get('http://hockeysite.romainbastien.com/admin/players')
        .then((response) => {
          context.commit('setPlayers', response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },

    createPlayer(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-player', {
        name: payload.player.name,
        team1_id: payload.player.team1_id,
        team2_id: payload.player.team2_id,
        position: payload.player.position,
        picture: payload.player.picture,
        experience: payload.player.experience,
        palmares: payload.player.palmares
      })
        .then(() => {
          console.log('upload done')
        })
        .catch((e) => {
          console.log(e)
        })
    },

    updatePlayer(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/update-player', {
        id: payload.player.player.id,
        name: payload.player.player.name,
        team1_id: payload.player.player.team1_id,
        team2_id: payload.player.player.team2_id,
        position: payload.player.player.position,
        picture: payload.player.player.picture,
        experience: payload.player.player.experience
      })

      payload.palmaresToDelete.forEach((id) =>{
        axios.post('http://hockeysite.romainbastien.com/admin/delete-palmares', {
          id: id
        })
      })

      payload.player.palmares.forEach((palmares) => {
        if (!palmares.id){
          axios.post('http://hockeysite.romainbastien.com/admin/add-palmares', {
            player_id: palmares.player_id,
            content: palmares.content
          })
        }
      })

      context.commit("setUpdatedPlayer", payload.player)
    },

    deletePlayer(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/delete-player', {
        id: payload.player.id
      })
        .then(() => {
          context.commit('deletePlayer', payload.player)
        })
    },

    playerToUpdate(context, payload){
      context.commit('setPlayerToUpdate', payload)
    }
  }
}
