import axios from 'axios'

export default {
  state: {

    galleries: [],
    galleryToUpdate: null

  },

  getters: {

    getGalleries(state, getters){
      return state.galleries.sort(function(a, b) {
        return a.id - b.id;
      });
    },

    getGalleryToUpdate(state, getters){
      return state.galleryToUpdate
    }

  },

  mutations: {

    setGalleries(state, payload){
      state.galleries = payload
    },

    setGalleryToUpdate(state, payload){
      state.galleryToUpdate = payload
    },

    addGallery(state, payload){
      state.galleryToUpdate = payload
    },

    addImageToGalleryToUpdate(state, payload){
      if (!state.galleryToUpdate.images){
        state.galleryToUpdate.images = []
      }
      state.galleryToUpdate.images.push(payload)
    },

    setUpdatedGallery(state, payload){
      state.galleries = state.galleries.filter(gallery => gallery.id != payload.id)
      state.galleries = [...state.galleries, payload]
    },

    deleteGallery(state, payload){
      state.galleries = state.galleries.filter(gallery => gallery.id != payload.id)
    },

    deleteImage(state, payload){
      state.galleryToUpdate.images = state.galleryToUpdate.images.filter(image => image.img_url != payload.img_url)
    }

  },

  actions: {
    fetchGalleries(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
      axios.get('http://hockeysite.romainbastien.com/admin/galleries')
        .then((response) => {
          context.commit('setGalleries', response.data)
        })
        .catch(e => {
          console.log(e)
        })
    },

    addGallery(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-gallery', {
        title: payload.gallery.title
      })
        .then(() => {
          context.commit('addGallery', payload.gallery)
        })
    },

    galleryToUpdate(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      context.commit('setGalleryToUpdate', payload)
    },

    addImageToGalleryToUpdate(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-image', {
        gallery_id: context.state.galleryToUpdate.id,
        img_url: payload.image.img_url
      })
        .then(() => {
          context.commit('addImageToGalleryToUpdate', payload.image)
        })
    },

    updateGallery(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/update-gallery', {
        id: payload.gallery.id,
        title: payload.gallery.title
      })
        .then(() => {
          context.commit('setUpdatedGallery', payload.gallery)
        })
    },

    deleteGallery(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/delete-gallery', {
        id: payload.gallery.id
      })
        .then(() => {
          context.commit('deleteGallery', payload.gallery)
        })
    },

    deleteImage(context, payload){
      axios.post('http://hockeysite.romainbastien.com/admin/delete-image', {
        id: payload.image.id
      })
        .then(() => {
          context.commit('deleteImage', payload.image)
        })
    }
  }
}
