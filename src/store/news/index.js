import axios from 'axios'

export default {
  state: {
    news: [],
    newsToUpdate: {}
  },

  getters: {
    getNews (state, getters) {
      return state.news
    },

    getNewsToUpdate(state, getters){
      return state.newsToUpdate
    }
  },

  mutations: {
    setNews (state, payload) {
      state.news = payload
    },

    setNewsToUpdate(state, payload) {
      state.newsToUpdate = payload
    },

    setUpdatedNews(state, payload) {
      state.news = state.news.filter(article => article.id != payload)
      state.news = [...state.news, payload]
    },

    deleteNews(state, payload) {
      state.news = state.news.filter(article => article.id != payload)
    }
  },

  actions: {
    fetchNews (context, payload) {
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload}`
      axios.get('http://hockeysite.romainbastien.com/admin/news')
        .then((response) => {
          context.commit('setNews', response.data)
        })
        .catch((e) => {
          console.log(e)
        })
    },

    createNews(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/add-news', {
        title: payload.news.title,
        description: payload.news.description,
        category: payload.news.category,
        featured_image_url: payload.news.featured_image_url,
        body: payload.news.body
      })
        .then(() => {
          console.log('upload done')
        })
        .catch((e) => {
          console.log(e)
        })
    },

    updateNews(context, payload){
        axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
        axios.post('http://hockeysite.romainbastien.com/admin/update-news', {
          id: payload.news.id,
          title: payload.news.title,
          description: payload.news.description,
          category: payload.news.category,
          featured_image_url: payload.news.featured_image_url,
          body: payload.news.body
        })
          .then(() => {
            context.commit("setUpdatedNews", payload.news)
          })
    },

    deleteNews(context, payload){
      axios.defaults.headers.common['Authorization'] = `Bearer ${payload.auth}`
      axios.post('http://hockeysite.romainbastien.com/admin/delete-news', {
        id: payload.news,
      })
        .then(() => {
          context.commit("deleteNews", payload.news)
        })
    },

    newsToUpdate(context, payload){
      context.commit('setNewsToUpdate', payload)
    }
  }
}
