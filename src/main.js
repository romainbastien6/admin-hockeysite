// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import { store } from './store'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee, faEdit, faTrashAlt, faPlus } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import 'bulma/css/bulma.css'

import Auth from '@okta/okta-vue'

Vue.use(Auth, {
  issuer: 'https://dev-265389.oktapreview.com/oauth2/default',
  client_id: '0oafwtx44obVRxBpn0h7',
  redirect_uri: 'http://localhost:8080/implicit/callback',
  scope: 'openid profile email'
})

library.add(faCoffee)
library.add(faEdit)
library.add(faTrashAlt)
library.add(faPlus)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
