import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import News from '@/components/news/News'
import NewsCreator from '@/components/news/NewsCreator'
import NewsUpdater from '@/components/news/NewsUpdater'
import Galleries from '@/components/galleries/Galleries'
import GalleryUpdater from '@/components/galleries/GalleryUpdater'
import GalleryCreator from '@/components/galleries/GalleryCreator'
import Videos from '@/components/videos/Videos'
import VideoUpdater from '@/components/videos/VideoUpdater'
import VideoCreator from '@/components/videos/VideoCreator'
import Standings from '@/components/standings/Standings'
import StandingUpdater from '@/components/standings/StandingUpdater'
import Players from '@/components/players/Players'
import PlayerUpdater from '@/components/players/PlayerUpdater'
import PlayerCreator from '@/components/players/PlayerCreator'
import Games from '@/components/games/Games'
import GameCreator from '@/components/games/GameCreator'
import GameUpdater from '@/components/games/GameUpdater'

import Auth from '@okta/okta-vue'

Vue.use(Auth, {
  issuer: 'https://dev-265389.oktapreview.com/oauth2/default',
  client_id: '0oafwtx44obVRxBpn0h7',
  redirect_uri: 'http://admin-hockeysite.romainbastien.com/implicit/callback',
  scope: 'openid profile email'
})

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/news',
      name: 'News',
      component: News,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/create-news',
      name: 'NewsCreator',
      component: NewsCreator,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/update-news',
      name: 'NewsUpdater',
      component: NewsUpdater,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/galleries',
      name: 'Galleries',
      component: Galleries,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/update-gallery',
      name: 'GalleryUpdater',
      component: GalleryUpdater,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/create-gallery',
      name: 'GalleryCreator',
      component: GalleryCreator,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/videos',
      name: 'Videos',
      component: Videos,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/create-video',
      name: 'VideoCreator',
      component: VideoCreator,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/update-video',
      name: 'VideoUpdater',
      component: VideoUpdater,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/standings',
      name: 'Standings',
      component: Standings,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/update-standing',
      name: 'StandingUpdater',
      component: StandingUpdater,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/players',
      name: 'Players',
      component: Players,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/update-player',
      name: 'PlayerUpdater',
      component: PlayerUpdater,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/create-player',
      name: 'PlayerCreator',
      component: PlayerCreator,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/games',
      name: 'Games',
      component: Games,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/create-game',
      name: 'GameCreator',
      component: GameCreator,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/update-game',
      name: 'GameUpdater',
      component: GameUpdater,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/implicit/callback',
      component: Auth.handleCallback()
    }
  ]
})

router.beforeEach(Vue.prototype.$auth.authRedirectGuard())

export default router
